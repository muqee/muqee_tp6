<?php
/**
 * @copyright   Copyright (c) http://muqee.com All rights reserved.
 *
 * CareyShop    服务类
 *
 * @author      andywu <andywu0823@qq.com>
 * @date        2021/4/21
 */

namespace Muqee\Tp6;

class MQService
{
    /**
     * 错误信息
     * @var string
     */
    public $error = '';

    /**
     * 设置错误信息
     * @access public
     * @param string $value 错误信息
     * @return false
     */
    public function setError(string $value)
    {
        $this->error = $value;
        return false;
    }

    /*
     * 获取错误信息
     * @access public
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
}
