<?php
/**
 * @copyright   Copyright (c) http://muqee.com All rights reserved.
 *
 * CareyShop    公共模型基类
 *
 * @author      andywu <andywu0823@qq.com>
 * @date        2021/4/21
 */

namespace Muqee\Tp6;

use think\exception\ValidateException;
use think\facade\Config;
use think\helper\Str;
use think\Model;

abstract class MQModel extends Model
{
    /**
     * 错误信息
     * @var string
     */
    protected $error = '';

    /**
     * 默认排序
     * @var array
     */
    protected $defaultOrder = [];

    /**
     * 固定排序
     * @var array
     */
    protected $fixedOrder = [];

    /**
     * 是否调整顺序
     * @var bool
     */
    protected $isReverse = false;

    /**
     * 检测是否存在相同值
     * @access public
     * @param array $map 查询条件
     * @return bool false:不存在
     */
    public static function checkUnique(array $map)
    {
        if (empty($map)) {
            return true;
        }

        $count = self::where($map)->count();
        if (is_numeric($count) && $count <= 0) {
            return false;
        }

        return true;
    }

    /**
     * 返回模型的错误信息
     * @access public
     * @return string|array
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * 设置模型错误信息
     * @access public
     * @param string $value 错误信息
     * @return bool
     */
    public function setError(string $value)
    {
        $this->error = $value;
        return false;
    }

    /**
     * 翻页搜索器
     * @access public
     * @param object $query
     * @param mixed  $value
     * @param mixed  $data
     */
    public function searchPageAttr($query, $value, $data)
    {
        $pageNo = isset($data['page_no']) ? $data['page_no'] : 1;
        $pageSize = isset($data['page_size']) ? $data['page_size'] : Config::get('app.list_rows');

        $query->page($pageNo, $pageSize);
    }

    /**
     * 排序搜索器
     * @access public
     * @param object $query
     * @param mixed  $value
     * @param mixed  $data
     */
    public function searchOrderAttr($query, $value, $data)
    {
        $order = [];
        if (isset($data['order_field']) || isset($data['order_type'])) {
            $order[$data['order_field']] = $data['order_type'];
        } else {
            $order = $this->defaultOrder;
        }

        if (!empty($this->fixedOrder)) {
            // 固定排序必须在前,否则将导致自定义排序无法覆盖
            $order = array_merge($this->fixedOrder, $order);
            if (!empty($data['order_field']) && $this->isReverse) {
                $order = array_reverse($order);
            }
        }

        if (!empty($order)) {
            $query->order($order);
        }
    }

    /**
     * 设置默认排序
     * @access public
     * @param array $order   默认排序
     * @param array $fixed   固定排序
     * @param bool  $reverse 是否调整顺序
     * @return $this
     */
    public function setDefaultOrder(array $order, $fixed = [], $reverse = false)
    {
        $this->defaultOrder = $order;
        $this->fixedOrder = $fixed;
        $this->isReverse = $reverse;

        return $this;
    }

    /**
     * 模型验证器
     * @access public
     * @param array|object $data     验证数据
     * @param string|null  $scene    场景名
     * @param bool         $clean    是否清理规则键值不存在的$data
     * @param string       $validate 验证器规则或类
     * @return bool
     */
    public function validateData(array &$data, $scene = null, $clean = false, $validate = '')
    {
        try {
            // 确定规则来源
            if (empty($validate)) {
                $class = '\\app\\common\\validate\\' . $this->getName();
                if ($scene) {
                    $v = new $class();
                    $v->extractScene($data, $scene, $clean, $this->getPk());
                } else {
                    $v = validate($class);
                }
            } else {
                $v = validate($validate);
                if ($scene) {
                    $v->extractScene($data, $scene, $clean, $this->getPk());
                }
            }

            if ($clean) {
                $keys = $v->getRuleKey();
                foreach ($data as $key => $value) {
                    if (!in_array($key, $keys, true)) {
                        unset($data[$key]);
                    }
                }

                unset($key, $value);
            }

            $v->failException(true)->check($data);
        } catch (ValidateException $e) {
            return $this->setError($e->getMessage());
        }

        return true;
    }

    /**
     * 替换数组中的驼峰键名为下划线
     * @access public
     * @param array  $name 需要修改的键名
     * @param array &$data 源数据
     */
    public static function keyToSnake(array $name, array &$data)
    {
        if (!is_array($name)) {
            return;
        }

        foreach ($name as $value) {
            foreach ($data as &$item) {
                if (!array_key_exists($value, $item)) {
                    continue;
                }

                $temp = $item[$value];
                unset($item[$value]);

                $item[Str::snake($value)] = $temp;
            }
        }
    }

    /**
     * 根据id获取filed值
     * @param $id
     * @param string $fieldName
     * @return mixed|string
     */
    public function getFieldById($id, $fieldName = 'name')
    {
        $ret = '';
        $map[] = [$this->pk, '=', $id];
        $item = $this->field($fieldName)->where($map)->find();
        if(!is_null($item)){
            $ret = $item[$fieldName];
        }
        return $ret;
    }

    /**
     * 根据字段条件获取值
     * @param $fieldName
     * @param $value
     * @return mixed|string
     */
    public function getItemByField($fieldName, $value)
    {
        $ret = [];
        $map[] = [$fieldName, '=', $value];
        $item = $this->where($map)->find();
        if(!is_null($item)){
            $ret = $item[$fieldName];
        }
        return $ret;
    }

    /**
     * 根据等于条件获取记录
     * @param array $where
     * @return array|Model|null
     */
    public function getItemByFields(array $where)
    {
        $ret = '';
        foreach($where as $field => $value){
            $map[] = [$field, '=', $value];
        }
        return $this->where($map)->find();
    }

    /**
     * 根据条件获取列表数据
     * @param array $data
     * @param array $order
     * @param bool $cache
     * @return array
     */
    public function getDataList(array $data, array $order = [], $cache = true)
    {
        // 搜索条件
        $map = [];
        foreach ($data as $key => $value){
            $map[] = [$key, '=', $value];
        }

        $result['total_result'] = $this->cache(true, null, $this->name)->where($map)->count();
        if ($result['total_result'] <= 0) {
            return $result;
        }

        // 实际查询
        $result['items'] = $this->setDefaultOrder($order, true)
            ->cache($cache, null, 'DeviceLog')
            ->where($map)
            ->withSearch(['page', 'order'], $data)
            ->select()
            ->toArray();

        return $result;
    }
}
